require('dotenv').config()
const fs = require('fs')
const parseCSV = require('csv-parser')
const request = require('request-promise')

;(async () => {
  try {
    const stream = fs.createReadStream('import.csv').pipe(parseCSV())
    console.time('parse')

    stream
      .on('data', async csvObj => {
        stream.pause()

        const line = {
          id: csvObj.id,
          key: csvObj.key,
          email: csvObj.email,
          dob: csvObj.dob,
          startDate: '',
          endDate: ''
        }

        if (csvObj.key === '') {
          const option = {
            // uri: process.env.PROD_URI,
            uri: process.env.UAT_URI,
            method: 'POST',
            body: [{
              rewardId: '85',
              startDate: '2019-03-01 00:00:00',
              endDate: '2019-03-31 23:59:59',
              prefix: 'BD19',
              email: csvObj.email
            }],
            json: true
          }

          const [response] = await request(option)

          if (response.error) {
            line.key = response.error
          } else if (response.key) {
            line.key = response.key
            line.startDate = '2019-03-01'
            line.endDate = '31/03/2019'
          }
        }

        fs.appendFile('results.csv', Object.values(line).toString() + '\n', err => {
          if (err) {
            console.log('Error saving to file!')
            console.log(err)
          } else {
            stream.resume()
          }
        })
      })
      .on('end', () => {
        console.log('Done compiling arr.')
        console.timeEnd('parse')
      })
  } catch (err) {
    console.log(err)
  }
})();
