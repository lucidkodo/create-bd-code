require('dotenv').config()
const fs = require('fs')
const parseCSV = require('csv-parser')
const request = require('request-promise')

;(async () => {
  try {
    const stream = fs.createReadStream('results (copy).csv').pipe(parseCSV())
    console.time('parse')

    stream
      .on('data', async csvObj => {
        stream.pause()

        const line = {
          id: csvObj.id,
          key: csvObj.key,
          email: csvObj.email,
          dob: csvObj.dob,
          startDate: csvObj.startdate,
          endDate: csvObj.enddate || '31/03/2019'
        }

        fs.appendFile('new-data.csv', Object.values(line).toString() + '\n', err => {
          if (err) {
            console.log('Error saving to file!')
            console.log(err)
          } else {
            stream.resume()
          }
        })
      })
      .on('end', () => {
        console.log('Done compiling arr.')
        console.timeEnd('parse')
      })
  } catch (err) {
    console.log(err)
  }
})();
